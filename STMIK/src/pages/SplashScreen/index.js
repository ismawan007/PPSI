import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Image} from 'react-native'
import { Logo } from '../../assets'


const Splash = ({ navigation }) => {

    //useEffect (() => {
        //setTimeout (() => {
           //navigation.replace('MainApp');
        //}, 3000)
    //}, [ navigation]);


    return (
        <View style ={styles.view}>
            <Image source={Logo} style={styles.logo} />
            <Text style={styles.text}>STMIK BI LIBRARY</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    view: {
        backgroundColor: '#66C8A3',
        flex: 1,
        alignItems:'center',
        justifyContent: 'center'
    },
    text:{
        color:'white',
        fontSize: 40,
        fontWeight: 'bold'
        
    },
    logo:{
        width: 200,
        height: 200,
        
    }

})
